const moment = require('moment');
module.exports = () => {
    const data = {
        users: []
    }
    const totalUsers = 50;
    for (let i = 0; i < totalUsers; i++) {
        data.users.push({
            id: i,
            name: `user ${i}`,
            title: `title ${i}`,
            milestone: (i%2 == 0)? 'high': 'low',
            date: moment.now()
        })
    }
    return data;
}